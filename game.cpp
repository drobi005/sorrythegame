//Demetrius Robinson
//CS250.0
//FinalProject_drobinso
#include <iostream>
#include <stdlib.h>     //used to call rand()
#include <ctime>       //used in conjunction with rand()
#include <cmath>
#include "game.h"
#include "Player.h"
#include "Computer.h"
using namespace std;

void game::playGame()
{
    srand(time(NULL));
    const int ROW = 11;
    const int COL = 11;
    const int maxPegs = 4;
    int playersTurn;
    int playerInput;
    int pegToMove;
    bool initialTurn = true;
    bool gameNotOver = true;
    bool inputCheck = true;
    bool plPegOnBoard = false; //condition for checking if a single player peg is on the game board
    bool greenPegOnBoard = false; //condition for checking if a single green peg is on the game board
    bool bluePegOnBoard = false; //condition for checking if a single blue peg is on the game board
    bool redPegOnBoard = false; //condition for checking if a single red peg is on the game board
    string namePlayer;
    game playEXODUS; //make a game object
    player1 firstPlayer; //make a player1 object
    computer greenCOM1; //make a green computer1 player
    computer blueCOM2; //make a blue computer2 player
    computer redCOM3; //make a red computer3 player
    playEXODUS.setAllCOMPegs(greenCOM1, blueCOM2, redCOM3); //set all default computer pegs
    playEXODUS.setALLCOMCorners(greenCOM1, blueCOM2, redCOM3); //set all default computer corners
    string (*gameBoard)[COL];   //create gameBoard on the heap
    gameBoard = new string[ROW][COL];
    string playerPegs[maxPegs] = {"1","2","3","4"}; //store player pegs
    string greenPegs[maxPegs] = {"g","g","g","g"}; //store computer player 1 pegs
    string bluePegs[maxPegs] = {"b","b","b","b"}; //store computer player 2 pegs
    string redPegs[maxPegs] = {"r","r","r","r"}; //store computer player 3 pegs
    string playerPegsOnBoard[maxPegs] = {".",".",".","."}; //player pegs active on board
    string grPegsOnBoard[maxPegs] = {".",".",".","."};; //store computer player 1 pegs active on board
    string blPegsOnBoard[maxPegs] = {".",".",".","."};; //store computer player 2 pegs active on board
    string rdPegsOnBoard[maxPegs] = {".",".",".","."};; //store computer player 3 pegs active on board

    //set all player names
    cout << "Enter your name" << endl; //prompt for user name
    cin >> namePlayer;
    firstPlayer.setNamePlayer(namePlayer);
    greenCOM1.setNameCOM("Green Corner (g)");
    blueCOM2.setNameCOM("Blue Corner (b)");
    redCOM3.setNameCOM("Red Corner (r)");
    system("CLS");  //clear screen
    playEXODUS.createBoard(gameBoard); //create the board

//Initial Loop - used to get a peg on the board
    while(initialTurn) //initial game phase-loop until a player rolls a 6
    {
        system("CLS");  //clear screen
        playersTurn = playEXODUS.turn(); //determine player turn
        playEXODUS.setDiceNumber(playEXODUS.rollTheDie()); //roll the die and call setter function

        playEXODUS.updateGameBoardCenter(gameBoard, playEXODUS.getDiceNumber()); //update the boards die value
        cout << firstPlayer.getNamePlayer() << "\t\t\t\t\t\t" << greenCOM1.getNameCOM()<< endl; //display player and green computer names
        playEXODUS.displayPlayerPegs(playerPegs, maxPegs); //display player pegs
        playEXODUS.displayGreenCOM1Pegs(greenPegs, maxPegs); //display green pegs

        playEXODUS.showGameBoard(gameBoard, ROW, COL); //display board
        cout << blueCOM2.getNameCOM() << "\t\t\t\t\t\t" << redCOM3.getNameCOM() << endl; //display blue and red names
        playEXODUS.displayBlueCOM2Pegs(bluePegs, maxPegs); //display blue pegs
        playEXODUS.displayRedCOM3Pegs(redPegs, maxPegs); //display red pegs


        if(playEXODUS.getDiceNumber() == 6)
        {
            switch(playersTurn)
            {
                case 1: //player 1's turn
                    do
                    {
                        cout << "Your turn. You rolled a " << playEXODUS.getDiceNumber() << endl;
                        cout << "Press 1 to move a peg to start" << endl;
                        cin >> playerInput;
                        if(playerInput == 1)
                        {
                            inputCheck = false;
                        }
                    }while(inputCheck);
                    inputCheck = true; //reset condition for future tests
                    cout << "Enter the index of the peg you want to move" << endl;//prompt user to pick a peg to place on board
                    cout << "Available pegs: ";
                    playEXODUS.displayPlayerPegs(playerPegs, maxPegs);
                    cout << endl;


                    //prompt user to enter valid index for peg to move on to board
                    do
                    {
                        cin >> playerInput;
                        if((playerInput > 0) && (playerInput <= 4))
                        {
                            inputCheck = false;
                        }
                        else
                        {
                            cout << "Invalid input!" << endl;
                        }
                    }while(inputCheck);
                    system("CLS");  //clear screen
                    //pass gameBoard and playerPegs array and player object into movePlayerPegToStart function
                    playEXODUS.movePlayerPegToStart(gameBoard, playerPegs, playerPegsOnBoard, maxPegs, playerInput, firstPlayer);

                    //display board
                    playEXODUS.updateGameBoardCenter(gameBoard, playEXODUS.getDiceNumber()); //update the boards die value
                    cout << firstPlayer.getNamePlayer() << "\t\t\t\t\t\t" << greenCOM1.getNameCOM()<< endl; //display player and green computer names
                    playEXODUS.displayPlayerPegs(playerPegs, maxPegs); //display player pegs
                    playEXODUS.displayGreenCOM1Pegs(greenPegs, maxPegs); //display green pegs

                    playEXODUS.showGameBoard(gameBoard, ROW, COL); //display board
                    cout << blueCOM2.getNameCOM() << "\t\t\t\t\t\t" << redCOM3.getNameCOM() << endl; //display blue and red names
                    playEXODUS.displayBlueCOM2Pegs(bluePegs, maxPegs); //display blue pegs
                    playEXODUS.displayRedCOM3Pegs(redPegs, maxPegs); //display red pegs

                    plPegOnBoard = true;
                    initialTurn = false;
                    break;
                case 2: //green's turn
                    system("CLS");  //clear screen
                    //prompt green user to randomly pick a peg to place on board
                    playerInput = (rand() % 4) + 1; //Green will pick random value from 1 through 4
                    playEXODUS.moveCOMPegToStart(gameBoard, greenPegs, grPegsOnBoard, maxPegs, playerInput, greenCOM1);

                    //display board
                    playEXODUS.updateGameBoardCenter(gameBoard, playEXODUS.getDiceNumber()); //update the boards die value
                    cout << firstPlayer.getNamePlayer() << "\t\t\t\t\t\t" << greenCOM1.getNameCOM()<< endl; //display player and green computer names
                    playEXODUS.displayPlayerPegs(playerPegs, maxPegs); //display player pegs
                    playEXODUS.displayGreenCOM1Pegs(greenPegs, maxPegs); //display green pegs

                    playEXODUS.showGameBoard(gameBoard, ROW, COL); //display board
                    cout << blueCOM2.getNameCOM() << "\t\t\t\t\t\t" << redCOM3.getNameCOM() << endl; //display blue and red names
                    playEXODUS.displayBlueCOM2Pegs(bluePegs, maxPegs); //display blue pegs
                    playEXODUS.displayRedCOM3Pegs(redPegs, maxPegs); //display red pegs

                    greenPegOnBoard = true;
                    initialTurn = false;
                    break;
                case 3: //blue's turn
                    system("CLS");  //clear screen
                    //prompt blue user to randomly pick a peg to place on board
                    playerInput = (rand() % 4) + 1; //blue will pick random value from 1 through 4
                    playEXODUS.moveCOMPegToStart(gameBoard, bluePegs, blPegsOnBoard, maxPegs, playerInput, blueCOM2);

                    //display board
                    playEXODUS.updateGameBoardCenter(gameBoard, playEXODUS.getDiceNumber()); //update the boards die value
                    cout << firstPlayer.getNamePlayer() << "\t\t\t\t\t\t" << greenCOM1.getNameCOM()<< endl; //display player and green computer names
                    playEXODUS.displayPlayerPegs(playerPegs, maxPegs); //display player pegs
                    playEXODUS.displayGreenCOM1Pegs(greenPegs, maxPegs); //display green pegs

                    playEXODUS.showGameBoard(gameBoard, ROW, COL); //display board
                    cout << blueCOM2.getNameCOM() << "\t\t\t\t\t\t" << redCOM3.getNameCOM() << endl; //display blue and red names
                    playEXODUS.displayBlueCOM2Pegs(bluePegs, maxPegs); //display blue pegs
                    playEXODUS.displayRedCOM3Pegs(redPegs, maxPegs); //display red pegs

                    bluePegOnBoard = true;
                    initialTurn = false;
                    break;
                case 4: //red's turn
                    system("CLS");  //clear screen
                    //prompt red user to randomly pick a peg to place on board
                    playerInput = (rand() % 4) + 1; //red will pick random value from 1 through 4
                    playEXODUS.moveCOMPegToStart(gameBoard, redPegs, rdPegsOnBoard, maxPegs, playerInput, redCOM3);

                    //display board
                    playEXODUS.updateGameBoardCenter(gameBoard, playEXODUS.getDiceNumber()); //update the boards die value
                    cout << firstPlayer.getNamePlayer() << "\t\t\t\t\t\t" << greenCOM1.getNameCOM()<< endl; //display player and green computer names
                    playEXODUS.displayPlayerPegs(playerPegs, maxPegs); //display player pegs
                    playEXODUS.displayGreenCOM1Pegs(greenPegs, maxPegs); //display green pegs

                    playEXODUS.showGameBoard(gameBoard, ROW, COL); //display board
                    cout << blueCOM2.getNameCOM() << "\t\t\t\t\t\t" << redCOM3.getNameCOM() << endl; //display blue and red names
                    playEXODUS.displayBlueCOM2Pegs(bluePegs, maxPegs); //display blue pegs
                    playEXODUS.displayRedCOM3Pegs(redPegs, maxPegs); //display red pegs

                    redPegOnBoard = true;
                    initialTurn = false;
                    break;

            }
        }
        else
        {
            initialTurn = true;
        }
    }//end of initialTurn loop



//Final Loop- used to get pegs moving around the board and determine a winner
    while(gameNotOver) //final game phase-loop until a winner is decided
    {
        system("CLS");  //clear screen
        playersTurn = playEXODUS.turn(); //determine player turn
        playEXODUS.setDiceNumber(playEXODUS.rollTheDie()); //roll the die and call setter function

        playEXODUS.updateGameBoardCenter(gameBoard, playEXODUS.getDiceNumber()); //update the boards die value
        cout << firstPlayer.getNamePlayer() << "\t\t\t\t\t\t" << greenCOM1.getNameCOM()<< endl; //display player and green computer names
        playEXODUS.displayPlayerPegs(playerPegs, maxPegs); //display player pegs
        playEXODUS.displayGreenCOM1Pegs(greenPegs, maxPegs); //display green pegs

        playEXODUS.showGameBoard(gameBoard, ROW, COL); //display board
        cout << blueCOM2.getNameCOM() << "\t\t\t\t\t\t" << redCOM3.getNameCOM() << endl; //display blue and red names
        playEXODUS.displayBlueCOM2Pegs(bluePegs, maxPegs); //display blue pegs
        playEXODUS.displayRedCOM3Pegs(redPegs, maxPegs); //display red pegs

        if(playEXODUS.getDiceNumber() == 6)
        {
            switch(playersTurn)
            {
                case 1: //player 1's turn
                    do
                    {
                        cout << "Your turn. You rolled a " << playEXODUS.getDiceNumber() << endl;
                        if(plPegOnBoard != false)
                        {
                            cout << "Press 1 to move a peg to start" << endl;
                            cout << "Press 2 to move a peg already on the board" << endl;
                            cin >> playerInput;
                            if(playerInput == 1 || playerInput == 2 )
                            {
                                inputCheck = false;
                            }
                        }
                        else
                        {
                            cout << "No pegs active on the board" << endl;
                            cout << "Press 1 to move a peg to start" << endl;
                            cin >> playerInput;
                            if(playerInput == 1)
                            {
                                inputCheck = false;
                            }
                        }
                    }while(inputCheck);
                    inputCheck = true; //reset condition for future tests
                    if(playerInput == 1)
                    {
                        cout << "Enter the index of the peg you want to move" << endl;//prompt user to pick a peg to place on board
                        cout << "Available pegs: ";
                        playEXODUS.displayPlayerPegs(playerPegs, maxPegs);
                        cout << endl;

                        //prompt user to enter valid index for peg to move on to board
                        do
                        {
                            cin >> playerInput;
                            if((playerInput > 0) && (playerInput <= 4))
                            {
                                inputCheck = false;
                            }
                            else
                            {
                                cout << "Invalid input!" << endl;
                            }
                        }while(inputCheck);
                        system("CLS");  //clear screen
                        //pass gameBoard and playerPegs array and player object into movePlayerPegToStart function
                        playEXODUS.movePlayerPegToStart(gameBoard, playerPegs, playerPegsOnBoard, maxPegs, playerInput, firstPlayer);
                    }
                    else if(playerInput == 2)
                    {
                        cout << "Enter the index of the peg you want to move" << endl;//prompt user to pick a peg to move across board
                        cout << "Pegs currently on the board: ";
                        playEXODUS.displayPlayerPegs(playerPegsOnBoard, maxPegs);
                        cout << endl;

                        //prompt user to enter valid index for peg to move on to board
                        do
                        {
                            cin >> playerInput;
                            if((playerInput > 0) && (playerInput <= 4))
                            {
                                //pegToMove = playerInput-1;
                                inputCheck = false;
                            }
                            else if(playerPegsOnBoard[playerInput-1] == ".")
                            {
                                cout << "Invalid input!" << endl;
                                cout << "Enter the index of the peg you want to move" << endl;//prompt user to pick a peg to move across board
                                cout << "Pegs currently on the board: ";
                                playEXODUS.displayPlayerPegs(playerPegsOnBoard, maxPegs);
                                cout << endl;
                            }
                        }while(inputCheck);
                        system("CLS");  //clear screen
                        playEXODUS.movePlayerPeg(gameBoard, playerPegsOnBoard, playerInput, playEXODUS.getDiceNumber(), firstPlayer);
                    }
                    //display board
                    playEXODUS.updateGameBoardCenter(gameBoard, playEXODUS.getDiceNumber()); //update the boards die value
                    cout << firstPlayer.getNamePlayer() << "\t\t\t\t\t\t" << greenCOM1.getNameCOM()<< endl; //display player and green computer names
                    playEXODUS.displayPlayerPegs(playerPegs, maxPegs); //display player pegs
                    playEXODUS.displayGreenCOM1Pegs(greenPegs, maxPegs); //display green pegs

                    playEXODUS.showGameBoard(gameBoard, ROW, COL); //display board
                    cout << blueCOM2.getNameCOM() << "\t\t\t\t\t\t" << redCOM3.getNameCOM() << endl; //display blue and red names
                    playEXODUS.displayBlueCOM2Pegs(bluePegs, maxPegs); //display blue pegs
                    playEXODUS.displayRedCOM3Pegs(redPegs, maxPegs); //display red pegs

                    plPegOnBoard = true;
                    initialTurn = false;
                    break;
            }
        }
        else if(playEXODUS.getDiceNumber() < 6)
        {

        }


        if(firstPlayer.getPegsInHome() == 4)
        {
            gameNotOver = false;
        }
        if(greenCOM1.getPegsInHome() == 4)
        {
            gameNotOver = false;
        }
        if(blueCOM2.getPegsInHome() == 4)
        {
            gameNotOver = false;
        }
        if(redCOM3.getPegsInHome() == 4)
        {
            gameNotOver = false;
        }

    }




    delete gameBoard;
}

void game::setAllCOMPegs(computer& greenCOM1, computer& blueCOM2, computer& redCOM3)
{
    //set green pegs
    greenCOM1.setPeg1("g");
    greenCOM1.setPeg2("g");
    greenCOM1.setPeg3("g");
    greenCOM1.setPeg4("g");

    //set blue pegs
    blueCOM2.setPeg1("b");
    blueCOM2.setPeg2("b");
    blueCOM2.setPeg3("b");
    blueCOM2.setPeg4("b");

    //set red pegs
    redCOM3.setPeg1("r");
    redCOM3.setPeg2("r");
    redCOM3.setPeg3("r");
    redCOM3.setPeg4("r");

}

void game::setALLCOMCorners(computer& greenCOM1, computer& blueCOM2, computer& redCOM3)
{
    //set green pegs corners
    greenCOM1.setStarterCornerRow(0);
    greenCOM1.setStarterCornerCol(10);

    //set blue pegs
    blueCOM2.setStarterCornerRow(10);
    blueCOM2.setStarterCornerCol(0);

    //set red pegs
    redCOM3.setStarterCornerRow(10);
    redCOM3.setStarterCornerCol(10);

}

void game::createBoard(string gameBoard[11][11])
{
    string makeGameBoard[11][11] = {
    {"*","*","*","*","*","*","*","*","*","*","*"},
    {"*","O"," "," "," "," "," "," "," ","O","*"},
    {"*"," ","O"," "," "," "," "," ","O"," ","*"},
    {"*"," "," ","O"," "," "," ","O"," "," ","*"},
    {"*"," "," "," ","O","_","O"," "," "," ","*"},
    {"*"," "," "," ","|","x","|"," "," "," ","*" },
    {"*"," "," "," ","O","_","O"," "," "," ","*"},
    {"*"," "," ","O"," "," "," ","O"," "," ","*"},
    {"*"," ","O"," "," "," "," "," ","O"," ","*"},
    {"*","O"," "," "," "," "," "," "," ","O","*"},
    {"*","*","*","*","*","*","*","*","*","*","*"}};

    //assign makeGameBoard elements to gameBoard
    for (int i=0; i<11; i++)
    {
        for(int j=0; j<11; j++)
        {
            gameBoard[i][j] = makeGameBoard[i][j];
        }
    }
}

void game::showGameBoard(string gameBoard[11][11], int ROW, int COL)
{
    for(int i=0; i<ROW; i++)
    {
        for(int j=0; j<COL; j++)
        {
            cout << gameBoard[i][j] << "     ";
        }
        cout << "\n\n\n\n";
    }
}

void game::updateGameBoardCenter(string gameBoard[11][11], int value)
{
    game boardValues; //make a game object
    //int value;
    int ROW = boardValues.getBoardCenterRow();
    int COL = boardValues.getBoardCenterCol();
    //value = boardValues.getDiceNumber();

    //based on the int value of the die update the boards center string value to match the die
    switch(value)
    {
        case 1:
            gameBoard[ROW][COL] = "1";
            break;
        case 2:
            gameBoard[ROW][COL] = "2";
            break;
        case 3:
            gameBoard[ROW][COL] = "3";
            break;
        case 4:
            gameBoard[ROW][COL] = "4";
            break;
        case 5:
            gameBoard[ROW][COL] = "5";
            break;
        case 6:
            gameBoard[ROW][COL] = "6";
            break;
    }
}


void game::displayPlayerPegs(string playerPegs[4], int maxPegs)
{
    for( int i=0; i<maxPegs; i++)
    {
        cout << playerPegs[i];
    }
}

void game::displayGreenCOM1Pegs(string greenPegs[4], int maxPegs)
{
    cout << "\t\t\t\t\t\t\t";
    for( int i=0; i<maxPegs; i++)
    {
        cout << greenPegs[i];
    }
    cout << endl;
}

void game::displayBlueCOM2Pegs(string bluePegs[4], int maxPegs)
{
    for( int i=0; i<maxPegs; i++)
    {
        cout << bluePegs[i];
    }
}

void game::displayRedCOM3Pegs(string redPegs[4], int maxPegs)
{
    cout << "\t\t\t\t\t\t\t";
    for( int i=0; i<maxPegs; i++)
    {
        cout << redPegs[i];
    }
    cout << endl;
}

inline
int game::rollTheDie()
{
    return (rand() % 6) + 1;
}

int game::turn()
{
    static int turn=1;
    int tempValue=0;

    switch(turn)
    {
        case 1:
            tempValue=turn;
            turn++;
            return tempValue;
        case 2:
            tempValue=turn;
            turn++;
            return tempValue;
        case 3:
            tempValue=turn;
            turn++;
            return tempValue;
        case 4:
            tempValue=turn;
            turn=1;
            return tempValue;
    }
}

void game::movePlayerPegToStart(string gameBoard[11][11], string playerPegs[4], string playerPegsOnBoard[4], int maxPegs, int pegIndex, const player1& firstPlayer)
{
    int boardRow = firstPlayer.getStarterCornerRow();
    int boardCol = firstPlayer.getStarterCornerCol();
    int realIndex = pegIndex;
    realIndex--;
    switch(pegIndex)
    {
        case 1:
            playerPegsOnBoard[realIndex]="1";
            break;
        case 2:
            playerPegsOnBoard[realIndex]="2";
            break;
        case 3:
            playerPegsOnBoard[realIndex]="3";
            break;
        case 4:
            playerPegsOnBoard[realIndex]="4";
            break;
    }
    pegIndex--; //decrement player input value to match playerPegs array index

    gameBoard[boardRow][boardCol] = playerPegs[pegIndex]; //move peg to starting corner
    playerPegs[pegIndex] =  ".";//remove element from playerPegs array;
}

void game::moveCOMPegToStart(string gameBoard[11][11], string COMPegs[4], string COMPegsOnBoard[4], int maxPegs, int pegIndex, const computer& playerCOM)
{
    int boardRow = playerCOM.getStarterCornerRow();
    int boardCol = playerCOM.getStarterCornerCol();
    int realIndex = pegIndex;
    realIndex--;
    switch(pegIndex)
    {
        case 1:
            COMPegsOnBoard[realIndex]="1";
            break;
        case 2:
            COMPegsOnBoard[realIndex]="2";
            break;
        case 3:
            COMPegsOnBoard[realIndex]="3";
            break;
        case 4:
            COMPegsOnBoard[realIndex]="4";
            break;
    }
    pegIndex--; //decrement player input value to match playerPegs array index
    gameBoard[boardRow][boardCol] = COMPegs[pegIndex]; //move peg to starting corner
    COMPegs[pegIndex] =  ".";//remove element from playerPegs array;
}

void game::movePlayerPeg(string gameBoard[11][11], string playerPegsOnBoard[4], int pegToMove, int dieValue, player1& firstPlayer)
{
    game movePeg; //make game object
    switch(pegToMove)
    {
        case 1:
            //call advancedMovePeg1 function
            movePeg.advancedMovePlayerPeg1(gameBoard, pegToMove, dieValue, firstPlayer);
            break;
        case 2:
            movePeg.advancedMovePlayerPeg2(gameBoard, pegToMove, dieValue, firstPlayer);
            break;
        case 3:
            movePeg.advancedMovePlayerPeg3(gameBoard, pegToMove, dieValue, firstPlayer);
            break;
        case 4:
            movePeg.advancedMovePlayerPeg4(gameBoard, pegToMove, dieValue, firstPlayer);
            break;
    }
}

void game::advancedMovePlayerPeg1(string gameBoard[11][11], int pegToMove, int dieValue , player1& firstPlayer)
{
    int realIndex = pegToMove;
    int spacesMoved;
    int totalSpacesMoved;
    bool pegFound = true;
    int currentROW;
    int currentCOL;
    int finalROW;
    int finalCOL;
    realIndex--;

    spacesMoved = firstPlayer.getPeg1SpacesMoved();
    totalSpacesMoved = spacesMoved + dieValue;
    firstPlayer.setPeg1SpacesMoved(totalSpacesMoved);

    if (totalSpacesMoved <= 10)
    {
        //search across top row on board for peg 1 then move peg across row based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg1() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentCOL >= 5 && currentCOL <=9) //help peg move around top right corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalROW = dieValue -  currentCOL;
                            gameBoard[abs(finalROW)][10] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1(); //peg is placed on column side on the right of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalCOL = dieValue + currentCOL;
                            gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1();
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentCOL < 5)
                    {
                        finalCOL = dieValue + currentCOL;
                        gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1();
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 10 && totalSpacesMoved <=20)
    {
        //search down right column on board for peg 1 then move peg down column based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg1() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentROW >= 5 && currentROW <=9) //help peg move around top right corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalCOL = dieValue -  currentROW;
                            gameBoard[10][10-finalCOL] = gameBoard[currentROW][currentCOL]; //peg is placed on bottom row side from the bottom right of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalROW = dieValue + currentROW;
                            gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW < 5)
                    {
                        finalROW = dieValue + currentROW;
                        gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 20 && totalSpacesMoved <=30)
    {
        //search across bottom row on board for peg 1 then move peg across row backwards based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg1() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentCOL <= 5 && currentCOL >=1) //help peg move around bottom left corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalROW = dieValue -  currentCOL;
                            gameBoard[10-abs(finalROW)][0] = gameBoard[currentROW][currentCOL]; //peg is placed on column side on the left of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalCOL = dieValue - currentCOL;
                            gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL];
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW > 5)
                    {
                        finalCOL = dieValue - currentCOL;
                        gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 30 && totalSpacesMoved >=39)
    {
        //search down right column on board for peg 1 then move peg down column based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg1() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentROW <= 5 && currentROW <=1) //help peg into home accurately
                    {
                        if(dieValue == 6)
                        {
                            //finalCOL = dieValue -  currentROW;
                            game::moveToPlayerPegHome(gameBoard, gameBoard[currentROW][currentCOL], dieValue, firstPlayer); //call moveToPlayerPegHome
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW > 5)
                    {
                        finalROW = dieValue - currentROW;
                        gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
}

void game::advancedMovePlayerPeg2(string gameBoard[11][11], int pegToMove, int dieValue , player1& firstPlayer)
{
    int realIndex = pegToMove;
    int spacesMoved;
    int totalSpacesMoved;
    bool pegFound = true;
    int currentROW;
    int currentCOL;
    int finalROW;
    int finalCOL;
    realIndex--;

    spacesMoved = firstPlayer.getPeg2SpacesMoved();
    totalSpacesMoved = spacesMoved + dieValue;
    firstPlayer.setPeg2SpacesMoved(totalSpacesMoved);

    if (totalSpacesMoved <= 10)
    {
        //search across top row on board for peg 1 then move peg across row based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg2() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentCOL >= 5 && currentCOL <=9) //help peg move around top right corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalROW = dieValue -  currentCOL;
                            gameBoard[abs(finalROW)][10] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1(); //peg is placed on column side on the right of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalCOL = dieValue + currentCOL;
                            gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1();
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentCOL < 5)
                    {
                        finalCOL = dieValue + currentCOL;
                        gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1();
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 10 && totalSpacesMoved <=20)
    {
        //search down right column on board for peg 1 then move peg down column based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg2() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentROW >= 5 && currentROW <=9) //help peg move around top right corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalCOL = dieValue -  currentROW;
                            gameBoard[10][10-finalCOL] = gameBoard[currentROW][currentCOL]; //peg is placed on bottom row side from the bottom right of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalROW = dieValue + currentROW;
                            gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW < 5)
                    {
                        finalROW = dieValue + currentROW;
                        gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 20 && totalSpacesMoved <=30)
    {
        //search across bottom row on board for peg 1 then move peg across row backwards based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg2() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentCOL <= 5 && currentCOL >=1) //help peg move around bottom left corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalROW = dieValue -  currentCOL;
                            gameBoard[10-abs(finalROW)][0] = gameBoard[currentROW][currentCOL]; //peg is placed on column side on the left of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalCOL = dieValue - currentCOL;
                            gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL];
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW > 5)
                    {
                        finalCOL = dieValue - currentCOL;
                        gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 30 && totalSpacesMoved >=39)
    {
        //search down right column on board for peg 1 then move peg down column based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg2() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentROW <= 5 && currentROW <=1) //help peg into home accurately
                    {
                        if(dieValue == 6)
                        {
                            //finalCOL = dieValue -  currentROW;
                            game::moveToPlayerPegHome(gameBoard, gameBoard[currentROW][currentCOL], dieValue, firstPlayer); //call moveToPlayerPegHome
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW > 5)
                    {
                        finalROW = dieValue - currentROW;
                        gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
}

void game::advancedMovePlayerPeg3(string gameBoard[11][11], int pegToMove, int dieValue , player1& firstPlayer)
{
    int realIndex = pegToMove;
    int spacesMoved;
    int totalSpacesMoved;
    bool pegFound = true;
    int currentROW;
    int currentCOL;
    int finalROW;
    int finalCOL;
    realIndex--;

    spacesMoved = firstPlayer.getPeg3SpacesMoved();
    totalSpacesMoved = spacesMoved + dieValue;
    firstPlayer.setPeg3SpacesMoved(totalSpacesMoved);

    if (totalSpacesMoved <= 10)
    {
        //search across top row on board for peg 1 then move peg across row based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg3() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentCOL >= 5 && currentCOL <=9) //help peg move around top right corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalROW = dieValue -  currentCOL;
                            gameBoard[abs(finalROW)][10] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1(); //peg is placed on column side on the right of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalCOL = dieValue + currentCOL;
                            gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1();
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentCOL < 5)
                    {
                        finalCOL = dieValue + currentCOL;
                        gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1();
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 10 && totalSpacesMoved <=20)
    {
        //search down right column on board for peg 1 then move peg down column based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg3() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentROW >= 5 && currentROW <=9) //help peg move around top right corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalCOL = dieValue -  currentROW;
                            gameBoard[10][10-finalCOL] = gameBoard[currentROW][currentCOL]; //peg is placed on bottom row side from the bottom right of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalROW = dieValue + currentROW;
                            gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW < 5)
                    {
                        finalROW = dieValue + currentROW;
                        gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 20 && totalSpacesMoved <=30)
    {
        //search across bottom row on board for peg 1 then move peg across row backwards based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg3() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentCOL <= 5 && currentCOL >=1) //help peg move around bottom left corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalROW = dieValue -  currentCOL;
                            gameBoard[10-abs(finalROW)][0] = gameBoard[currentROW][currentCOL]; //peg is placed on column side on the left of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalCOL = dieValue - currentCOL;
                            gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL];
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW > 5)
                    {
                        finalCOL = dieValue - currentCOL;
                        gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 30 && totalSpacesMoved >=39)
    {
        //search down right column on board for peg 1 then move peg down column based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg3() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentROW <= 5 && currentROW <=1) //help peg into home accurately
                    {
                        if(dieValue == 6)
                        {
                            //finalCOL = dieValue -  currentROW;
                            game::moveToPlayerPegHome(gameBoard, gameBoard[currentROW][currentCOL], dieValue, firstPlayer); //call moveToPlayerPegHome
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW > 5)
                    {
                        finalROW = dieValue - currentROW;
                        gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
}

void game::advancedMovePlayerPeg4(string gameBoard[11][11], int pegToMove, int dieValue , player1& firstPlayer)
{
    int realIndex = pegToMove;
    int spacesMoved;
    int totalSpacesMoved;
    bool pegFound = true;
    int currentROW;
    int currentCOL;
    int finalROW;
    int finalCOL;
    realIndex--;

    spacesMoved = firstPlayer.getPeg4SpacesMoved();
    totalSpacesMoved = spacesMoved + dieValue;
    firstPlayer.setPeg4SpacesMoved(totalSpacesMoved);

    if (totalSpacesMoved <= 10)
    {
        //search across top row on board for peg 1 then move peg across row based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg4() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentCOL >= 5 && currentCOL <=9) //help peg move around top right corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalROW = dieValue -  currentCOL;
                            gameBoard[abs(finalROW)][10] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1(); //peg is placed on column side on the right of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalCOL = dieValue + currentCOL;
                            gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1();
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentCOL < 5)
                    {
                        finalCOL = dieValue + currentCOL;
                        gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL]; //firstPlayer.getPeg1();
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 10 && totalSpacesMoved <=20)
    {
        //search down right column on board for peg 1 then move peg down column based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg4() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentROW >= 5 && currentROW <=9) //help peg move around top right corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalCOL = dieValue -  currentROW;
                            gameBoard[10][10-finalCOL] = gameBoard[currentROW][currentCOL]; //peg is placed on bottom row side from the bottom right of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalROW = dieValue + currentROW;
                            gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW < 5)
                    {
                        finalROW = dieValue + currentROW;
                        gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 20 && totalSpacesMoved <=30)
    {
        //search across bottom row on board for peg 1 then move peg across row backwards based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg4() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentCOL <= 5 && currentCOL >=1) //help peg move around bottom left corner accurately
                    {
                        if(dieValue == 6)
                        {
                            finalROW = dieValue -  currentCOL;
                            gameBoard[10-abs(finalROW)][0] = gameBoard[currentROW][currentCOL]; //peg is placed on column side on the left of the board
                            gameBoard[currentROW][currentCOL] = "*";
                        }
                        else
                        {
                            finalCOL = dieValue - currentCOL;
                            gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL];
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW > 5)
                    {
                        finalCOL = dieValue - currentCOL;
                        gameBoard[currentROW][finalCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
    else if (totalSpacesMoved > 30 && totalSpacesMoved >=39)
    {
        //search down right column on board for peg 1 then move peg down column based on die value
        for(int i=0; i<11; i++)
        {
            for(int j=0; j<11; j++)
            {
                if(firstPlayer.getPeg4() == gameBoard[i][j])
                {
                    currentROW = i;
                    currentCOL = j;
                    //gameBoard[currentROW][currentCOL] = "*"; //restore the board where the peg
                    //check for COM pegs
                    if(currentROW <= 5 && currentROW <=1) //help peg into home accurately
                    {
                        if(dieValue == 6)
                        {
                            //finalCOL = dieValue -  currentROW;
                            game::moveToPlayerPegHome(gameBoard, gameBoard[currentROW][currentCOL], dieValue, firstPlayer); //call moveToPlayerPegHome
                            gameBoard[currentROW][currentCOL] = "*";
                        }

                    }
                    else if(currentROW > 5)
                    {
                        finalROW = dieValue - currentROW;
                        gameBoard[finalROW][currentCOL] = gameBoard[currentROW][currentCOL];
                        gameBoard[currentROW][currentCOL] = "*";
                    }
                }
            }
        }
    }
}

void game::moveToPlayerPegHome(string gameBoard[11][11], string peg, int dieValue, player1& firstPlayer)
{
    if(dieValue == 6)
    {
        if(gameBoard[4][4] == "O")
        {
            gameBoard[4][4] = peg;
        }
    }
    else if(dieValue < 6)
    {
        if(gameBoard[3][3] == "O" && gameBoard[4][4] != "O")
        {
            gameBoard[3][3] = peg;
        }
        else if(gameBoard[2][2] == "O" && gameBoard[4][4] != "O" && gameBoard[3][3] != "O")
        {
            gameBoard[2][2] = peg;
        }
        else if(gameBoard[1][1] == "O" && gameBoard[4][4] != "O" && gameBoard[3][3] != "O" && gameBoard[2][2] != "O")
        {
            gameBoard[1][1] = peg;
            firstPlayer.setPegsInHome(4);
        }
    }


}

void game::moveCOMPeg(string gameBoard[11][11], string COMPegsOnBoard[4], int pegToMove, int dieValue, computer& playerCOM)
{

}

