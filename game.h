#ifndef GAME_H_INCLUDED
#define GAME_H_INCLUDED
#include "Player.h"
#include "Computer.h"
using namespace std;

class game
{
    private:
        int diceNumber;
        int homeCountPlayer;
        int homeCountCOM1;
        int homeCountCOM2;
        int homeCountCOM3;
        int boardCenterRow; //set to 5-used for updating the center value of the game board
        int boardCenterCol; //set to 5
        string restorePerimeter;
        string restoreHome;
    public:
        game(int a=0, int b=0, int c=0, int d=0, int e=0, int f=5, int g=5, string rP="*", string rH="O")
        {
            diceNumber=a;
            homeCountPlayer=b;
            homeCountCOM1=c;
            homeCountCOM2=d;
            homeCountCOM3=e;
            boardCenterRow=f;
            boardCenterCol=g;
            restorePerimeter=rP;
            restoreHome=rH;
        }
        void playGame();

        int getDiceNumber() const {return diceNumber;}
        int getHomeCountPlayer() const {return homeCountPlayer;}
        int getHomeCountCOM1() const {return homeCountCOM1;}
        int getHomeCountCOM2() const {return homeCountCOM2;}
        int getHomeCountCOM3() const {return homeCountCOM3;}
        int getBoardCenterRow() const {return boardCenterRow;}
        int getBoardCenterCol() const {return boardCenterCol;}
        string getRestorePerimeter() const {return restorePerimeter;}
        string getRestoreHome() const {return restoreHome;}

        void setDiceNumber(int n) {diceNumber=n;}
        void setHomeCountPlayer(int c) {homeCountPlayer=c;}
        void setHomeCountCOM1(int c) {homeCountCOM1=c;}
        void setHomeCountCOM2(int c) {homeCountCOM2=c;}
        void setHomeCountCOM3(int c) {homeCountCOM3=c;}
        void setAllCOMPegs(computer&, computer&, computer&); //pass all 3 computer player objects and set their pegs
        void setALLCOMCorners(computer&, computer&, computer&); //pass all 3 computer player objects and set their starter corners

        void createBoard(string[11][11]);
        void showGameBoard(string[11][11], int, int);
        void updateGameBoardCenter(string[11][11], int); //update the boards center value
        void updateGameBoardPegs(string[11][11], int, int); //update the boards perimeter
        void updateGameBoardHomes(string[11][11], int, int); //update the homes for a players peg
        void displayPlayerPegs(string[4], int);
        void displayGreenCOM1Pegs(string[4], int);
        void displayBlueCOM2Pegs(string[4], int);
        void displayRedCOM3Pegs(string[4], int);


        int turn(); //based on whose turn is currently active. May change to int due to dice roll
        int rollTheDie(); //randomly generate number between 1-6
        void movePlayerPegToStart(string[11][11], string[4], string[4], int, int, const player1&); //move player peg to starting corner
        void moveCOMPegToStart(string[11][11], string[4], string[4], int, int, const computer&); //move computer peg to starting corner
        void movePlayerPeg(string[11][11], string[4], int, int, player1&);
        void advancedMovePlayerPeg1(string[11][11], int, int, player1&);
        void advancedMovePlayerPeg2(string[11][11], int, int, player1&);
        void advancedMovePlayerPeg3(string[11][11], int, int, player1&);
        void advancedMovePlayerPeg4(string[11][11], int, int, player1&);
        void moveToPlayerPegHome(string[11][11], string, int, player1&);
        void moveCOMPeg(string[11][11], string[4], int, int, computer&);
        bool checkForWinner();
        bool checkForKill();
};

#endif // GAME_H_INCLUDED
