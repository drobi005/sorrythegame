#ifndef COMPUTER_H_INCLUDED
#define COMPUTER_H_INCLUDED
using namespace std;

class computer
{
    private:
        string name;
        int peg1SpacesMoved;
        int peg2SpacesMoved;
        int peg3SpacesMoved;
        int peg4SpacesMoved;
        int starterCornerRow;
        int starterCornerCol;
        int homeRow;
        int homeCol;
        int pegsInHome;
        string peg1;
        string peg2;
        string peg3;
        string peg4;
    public:
        computer(string nameCOM="COM", int a=0, int b=0, int c=0, int d=0, int e=0, int f=0, int g=0, string h=".", int i=0, int j=0)
        {
            name=nameCOM;
            peg1SpacesMoved=a;
            peg2SpacesMoved=b;
            peg3SpacesMoved=c;
            peg4SpacesMoved=d;
            homeRow=e;
            homeCol=f;
            pegsInHome=g; //must be 4 to determine winner
            peg1=h;
            peg2=h;
            peg3=h;
            peg4=h;
            starterCornerRow=i;
            starterCornerCol=j;
        }
        string getNameCOM() const {return name;}
        int getPeg1SpacesMoved() const {return peg1SpacesMoved;}
        int getPeg2SpacesMoved() const {return peg2SpacesMoved;}
        int getPeg3SpacesMoved() const {return peg3SpacesMoved;}
        int getPeg4SpacesMoved() const {return peg4SpacesMoved;}
        int getStarterCornerRow() const {return starterCornerRow;}
        int getStarterCornerCol() const {return starterCornerCol;}
        int gethomeRow() const {return homeRow;}
        int gethomeCol() const {return homeCol;}
        int getPegsInHome() const {return pegsInHome;}
        string getPeg1() const {return peg1;}
        string getPeg2() const {return peg2;}
        string getPeg3() const {return peg3;}
        string getPeg4() const {return peg4;}

        void setNameCOM(string n) {name=n;}
        void setPeg1SpacesMoved(int s) {peg1SpacesMoved=s;}
        void setPeg2SpacesMoved(int s) {peg2SpacesMoved=s;}
        void setPeg3SpacesMoved(int s) {peg3SpacesMoved=s;}
        void setPeg4SpacesMoved(int s) {peg4SpacesMoved=s;}
        void setStarterCornerRow(int c) {starterCornerRow=c;}
        void setStarterCornerCol(int c) {starterCornerCol=c;}
        void sethomeRow();
        void sethomeCol();
        void setPegsInHome(int h) {pegsInHome=h;}
        void setPeg1(string p1) {peg1=p1;}
        void setPeg2(string p2) {peg2=p2;}
        void setPeg3(string p3) {peg3=p3;}
        void setPeg4(string p4) {peg4=p4;}


        void behaviorCOM();
        int pickPeg(); //randomly generate number from 1-4

};

#endif // COMPUTER_H_INCLUDED
