//Demetrius Robinson
//CS250.0
//FinalProject_drobinso
#include <iostream>
#include "game.h"
#include "Player.h"
#include "Computer.h"
using namespace std;

int main()
{
    game play; //create game object
    play.playGame(); //call game function
    return 0;
}
