#ifndef PLAYER_H_INCLUDED
#define PLAYER_H_INCLUDED
using namespace std;

class player1
{
    private:
        string name;
        int peg1SpacesMoved;
        int peg2SpacesMoved;
        int peg3SpacesMoved;
        int peg4SpacesMoved;
        int starterCornerRow;
        int starterCornerCol;
        int pegsOffBoard;
        int pegsInHome;

        string peg1;
        string peg2;
        string peg3;
        string peg4;
    public:
        player1(string namePlayer="Player1", int a=0, int b=0, int c=0, int d=0, int e=0, int f=0, int g=4, string p1="1", string p2="2", string p3="3", string p4="4")
        {
            name=namePlayer;
            peg1SpacesMoved=a;
            peg2SpacesMoved=b;
            peg3SpacesMoved=c;
            peg4SpacesMoved=d;
            starterCornerRow=e;
            starterCornerCol=f;
            pegsOffBoard=g;
            peg1=p1;
            peg2=p2;
            peg3=p3;
            peg4=p4;
            pegsInHome=a; //must be 4 to determine winner
        }

        string getNamePlayer() const {return name;}
        int getPeg1SpacesMoved() const {return peg1SpacesMoved;}
        int getPeg2SpacesMoved() const {return peg2SpacesMoved;}
        int getPeg3SpacesMoved() const {return peg3SpacesMoved;}
        int getPeg4SpacesMoved() const {return peg4SpacesMoved;}
        int getStarterCornerRow() const {return starterCornerRow;}
        int getStarterCornerCol() const {return starterCornerCol;}
        int getPegsInHome() const {return pegsInHome;}
        string getPeg1() const {return peg1;}
        string getPeg2() const {return peg2;}
        string getPeg3() const {return peg3;}
        string getPeg4() const {return peg4;}

        void setNamePlayer(string n) {name=n;}
        void setPegsInHome(int h) {pegsInHome=h;}
        void setPeg1SpacesMoved(int s) {peg1SpacesMoved=s;}
        void setPeg2SpacesMoved(int s) {peg2SpacesMoved=s;}
        void setPeg3SpacesMoved(int s) {peg3SpacesMoved=s;}
        void setPeg4SpacesMoved(int s) {peg4SpacesMoved=s;}
        //void setHomeRow();
        //void setHomeCol();

        int pickPeg();
        void reset(); //if peg# is killed throw back into array and reset peg# back to 0
};

#endif // PLAYER_H_INCLUDED
